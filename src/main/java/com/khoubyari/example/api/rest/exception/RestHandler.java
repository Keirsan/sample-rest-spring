package com.khoubyari.example.api.rest.exception;

import com.khoubyari.example.domain.RestErrorInfo;
import com.khoubyari.example.exception.DataFormatException;
import com.khoubyari.example.exception.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * This class is meant to be extended by all REST resource "controllers".
 * It contains exception mapping and other common REST API functionality
 */
@ControllerAdvice
public class RestHandler {

    private final static Logger LOGGER = LoggerFactory.getLogger(RestHandler.class);

    @ExceptionHandler(DataFormatException.class)
    public ResponseEntity handleDataStoreException(DataFormatException ex) {
        LOGGER.info("Converting Data Store exception to RestResponse : " + ex.getMessage());

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new RestErrorInfo(ex, "Something wrong."));
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity handleResourceNotFoundException(ResourceNotFoundException ex) {
        LOGGER.info("ResourceNotFoundException handler:" + ex.getMessage());

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new RestErrorInfo(ex, "Sorry I couldn't find it."));
    }

}